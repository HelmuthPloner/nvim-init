# nvim-init

Configuration of neovim

~/.zshrc
~~~~
alias vim=nvim
export VIMCONFIG=~/.config/nvim
~~~~

~~~~
$ mkdir -p $VIMCONFIG/pack/minpac/opt
$ cd $VIMCONFIG/pack/minpac/opt
$ git clone https://github.com/k-takata/minpac.git
~~~~

~~~~
packadd minpac
call minpac#init()
~~~~