set wildmode=list:longest
set number
if has('mouse')
  set mouse=a
endif

if filereadable('/usr/share/dict/words')
  set dictionary+=/usr/share/dict/words
endif

packadd minpac

call minpac#init()

" A minimal package manager for Vim 8 (and Neovim)
call minpac#add('k-takata/minpac', {'type': 'opt'})

" lean & mean status/tabline for vim that's light as air
call minpac#add('vim-airline/vim-airline')

" A collection of themes for vim-airline
call minpac#add('vim-airline/vim-airline-themes')

" Better whitespace highlighting for Vim
call minpac#add('ntpeters/vim-better-whitespace')

" A tree explorer plugin for vim
call minpac#add('scrooloose/nerdtree')

" A command-line fuzzy finder
call minpac#add('junegunn/fzf')
call minpac#add('junegunn/fzf.vim')

" a Git wrapper so awesome, it should be illegal"
call minpac#add('tpope/vim-fugitive')

" Show a diff via Vim sign column.
call minpac#add('mhinz/vim-signify')

" plugin for vim to switch b/t source and header files
call minpac#add('kris2k/a.vim')

" Finding files semantically
call minpac#add('tpope/vim-projectionist')

" Projections + Fuzzy Finder = ❤️
call minpac#add('c-brenn/fuzzy-projectionist.vim')

" quoting/parenthesizing made simple
call minpac#add('tpope/vim-surround')

" Asynchronous linting/fixing for Vim and Language Server Protocol (LSP) integration
call minpac#add('w0rp/ale')

" Advanced syntax highlighting for GNU as (AT&T) for *86 CPUs
call minpac#add('shirk/vim-gas')

" Additional Vim syntax highlighting for C++ (including C++11/14/17)
call minpac#add('octol/vim-cpp-enhanced-highlight')

" Vim plugin for clang-format
call minpac#add('rhysd/vim-clang-format')

" QML SYNTAX FILE FOR VIM
call minpac#add('peterhoeg/vim-qml')

" A Vim plugin for Windows PowerShell support
call minpac#add('pprovost/vim-ps1')

" A code-completion engine for Vim
call minpac#add('ycm-core/YouCompleteMe')

" Use RipGrep in Vim and display results in a quickfix list
call minpac#add('jremmen/vim-ripgrep')

" Asynchronous build and test dispatcher
call minpac#add('tpope/vim-dispatch')

" Adds neovim support to vim-dispatch
call minpac#add('radenling/vim-dispatch-neovim')

if has("autocmd")

    augroup initEx

        autocmd!

        " Restore cursor position
        autocmd BufReadPost *
                    \ if 1 < line("'\"") && line("'\"") <= line("$") && &filetype != "gitcommit" |
                    \   execute "normal! g`\"" |
                    \ endif

        " indentation
        autocmd FileType cpp,json setlocal smartindent shiftwidth=4 tabstop=4 expandtab smarttab

    augroup END

endif

set cinoptions=:0,l1,g0,N-s,t0,j1,J1
let c_no_curly_error=1

let g:airline_theme='sol'
let g:airline_powerline_fonts = 1

command! PackUpdate call minpac#update()
command! PackClean call minpac#clean()

if executable('rg')
    nnoremap <C-k> :<C-u>call fzf#run({'source': 'rg --files', 'sink': 'e'})<CR>
else
    nnoremap <C-k> :<C-u>GFiles<CR>
endif

noremap <C-T>o :tabonly<CR>
noremap <C-T>c :tabclose<CR>
noremap <C-T><C-T> :tabnext<CR>

nnoremap <silent> <F4> :A<CR>
inoremap <silent> <F4> <Esc>:A<CR>

nnoremap <F6> :cnext<CR>
if has('mac')
    nnoremap <F18> :cprevious<CR>
else
    nnoremap <S-F6> :cprevious<CR>
end

" vim:ts=4:sw=4:et:sta
